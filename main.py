import math
import random

WIDTH=800
HEIGHT=600

class loopInFrameMixin():
        def loop_in_frame(self):
            if self.x > WIDTH:
                self.x = 0

            if self.x < 0:
                self.x = WIDTH

            if self.y > HEIGHT:
                self.y = 0

            if self.y < 0:
                self.y = HEIGHT

class Starship(Actor, loopInFrameMixin):
    PACMAN_MAX_SPEED = 3

    def __init__(self, image, pos):
        super().__init__(image, pos)
        self.angle = 90
        self.speed = 0
        self.x_direction = 0
        self.y_direction = 0

        self.bullets=[]

    def update_position(self):
        self.x_direction = math.sin(math.radians(self.angle))
        self.y_direction = math.cos(math.radians(self.angle))

        self.x += self.x_direction * self.speed
        self.y += self.y_direction * self.speed

    def shoot(self):
        bullet = Bullet(self)
        self.bullets.append(bullet)
        sounds.bullet.play()

pacman = Starship('pacman', (400,300))

class Pizza(Actor, loopInFrameMixin):
    def __init__(self):
        x_init = random.uniform(0,WIDTH)
        y_init = random.uniform(0,HEIGHT)
        super().__init__('pizza', (x_init,y_init))
        self.angle = 0
        self.speed = random.choice([1,2,3])
        self.x_direction = random.uniform(-1,1)
        self.y_direction = random.uniform(-1,1)

    def update_position(self):
        self.x += self.x_direction * self.speed
        self.y += self.y_direction * self.speed


pizzas = []
for _ in range(1,5):
    pizzas.append(Pizza())

class Bullet(Actor, loopInFrameMixin):
    BULLET_SPEED = 3

    def __init__(self, pacman):
        super().__init__('bullet', (pacman.x,pacman.y))
        self.angle = pacman.angle
        self.speed = self.BULLET_SPEED
        self.x_direction = pacman.x_direction
        self.y_direction = pacman.y_direction

    def update_position(self):
        self.x += self.x_direction * self.speed
        self.y += self.y_direction * self.speed

bonus = None
class Bonus(Actor):
    def __init__(self):
        x_init = random.uniform(0,WIDTH)
        y_init = random.uniform(0,HEIGHT)
        super().__init__('big-pizza', (x_init, y_init))

    @classmethod
    def set_bonus(cls):
        global bonus
        bonus = cls()

    @classmethod
    def schedule(cls):
        clock.schedule(cls.set_bonus, random.choice([1,2,3,4]))

Bonus.schedule()

def draw():
    screen.fill((59,59,59))
    pacman.draw()
    for pizza in pizzas:
        pizza.draw()

    for bullet in pacman.bullets:
        bullet.draw()

    if bonus:
        bonus.draw()

def update():
    global bonus

    if keyboard.up:
        if pacman.speed < Starship.PACMAN_MAX_SPEED:
            pacman.speed += 1
    else:
        if pacman.speed>0:
            pacman.speed -= 0.25


    if keyboard.left:
        pacman.angle += 2

    if keyboard.right:
        pacman.angle -= 2

    pacman.update_position()
    pacman.loop_in_frame()

    for pizza in pizzas:
        pizza.update_position()
        pizza.loop_in_frame()

    pizza_id = pacman.collidelist(pizzas)
    if pizza_id >= 0:
        pizzas.pop(pizza_id)

    for bullet in pacman.bullets:
        bullet.update_position()

        collided_pizza_id = bullet.collidelist(pizzas)
        if collided_pizza_id >= 0:
            pizzas.pop(collided_pizza_id)
            pacman.bullets.remove(bullet)

            new_pizza = Pizza()
            pizzas.append(new_pizza)

        if bonus and bullet.colliderect(bonus):
            bonus = None
            Bonus.schedule()

    if bonus and pacman.colliderect(bonus):
        bonus = None
        Bonus.schedule()


def on_key_down(key):
    if key == keys.SPACE:
        pacman.shoot()
    if key == keys.ESCAPE:
        sys.exit()
